
const FIRST_NAME = "Diana Gabriela";
const LAST_NAME = "Feighel";
const GRUPA = "1077";

/**
 * Make the implementation here
 */
function numberParser(value) {
    if((value<Number.MAX_SAFE_INTEGER) && (value>=Number.MIN_SAFE_INTEGER) && (value!==NaN)
     && (value!=Infinity) && (value!=-Infinity) )
        return parseInt(value);
    else
        return NaN;
}


module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    numberParser
}

